import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class ChangePassword extends StatefulWidget {
  const ChangePassword({Key key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController emailController = TextEditingController();

  bool isLoading = false;
  _validateAndProceed() async {
    var value = emailController.text;
    if(value != null && value.contains("@") && value.contains(".")) {
      setState(() {
        isLoading = true;
      });

      try {

        await FirebaseAuth.instance.sendPasswordResetEmail(email: value);

        emailController.clear();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("A reset password link sent to you email.")));

        Navigator.of(context).pop();

        setState(() {
          isLoading = false;
        });

      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
      }
      catch (e) {
        setState(() {
          isLoading = false;
        });
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error sending email.")));
      }


    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Change Password'),
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 35),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            //mainAxisSize: MainAxisSize.min,
            children: [SizedBox(height: 100,),
              TextField(
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                // validator: (value) => (value != null && value.contains("@") && value.contains(".")) ? null : "Email must be valid",
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                    hintText: "Email",
                    hintStyle: Theme.of(context).textTheme.bodyText1.apply(color: Color(0xff2b2a2a)),
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Colors.black,
                      ),)
                  //border: InputBorder.none
                ),
              ),

              SizedBox(height: 20,),

              this.isLoading
                  ? SizedBox(height: 35, child: CircularProgressIndicator())
                  : ElevatedButton(
                onPressed: () {
                  _validateAndProceed();
                },
                style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),
                child: Center(
                  child: Text("Send Reset Password Link"),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
