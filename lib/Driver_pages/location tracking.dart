import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/model/BusRoute.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';
import 'package:location/location.dart' as l;
class LocationTrack extends StatefulWidget {
  MyUser currentUser;
  Bus bus;

  LocationTrack({Key key, this.currentUser, this.bus, List<BusRoute> routes}) : super(key: key);

  @override
  _LocationTrackState createState() => _LocationTrackState();
}

class _LocationTrackState extends State<LocationTrack> {
  MyUser currentUser;
  Bus bus;
  bool isTracking = false;

  List<Marker> allMarkers = [];
  GoogleMapController _mapController;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    this.currentUser = widget.currentUser;
    this.bus = widget.bus;

    super.initState();
    getLocationPermission();
  }
  @override

  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      key: this.scaffoldKey,
      body: Stack(
        children: [

          Container(
            width: size.width,
            height: size.height,
            // color: Colors.indigo,
            child: GoogleMap(
              onMapCreated: (GoogleMapController _controller) {
                this._mapController = _controller;
              },
              initialCameraPosition: CameraPosition(
                  target: LatLng(33.6780513, 73.1843443), zoom: 13
              ),
              onTap: (post){},
              mapType: MapType.normal,
              markers: Set.from(this.allMarkers),
            ),
          ),


          this.isTracking
              ? SizedBox()
              : Positioned(
            bottom: 5,
            right: 10,
            left: 10,
            child: ElevatedButton(
              onPressed: () async {
                //TODO: Start location tracking
                if(await getLocationPermission()) {
                  setState((){
                    this.isTracking = true;
                  });
                  setLocationListener();
                }
              },
              style: ElevatedButton.styleFrom(primary: Color(0xff2b2a2a),shape: StadiumBorder()),
              child: Center(
                child: Text("Start"),
              ),
            ),
          ),

        ],
      ),
    );

  }

  Future<bool> getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
      ///Granted
      ///We can Update location
      return true;
    } else {
      final _req = await l.Location().requestPermission();
      if(_req == l.PermissionStatus.granted) {
        ///Granted
        ///We can Update location
        return true;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: Text("You must allow permission to update location")
            )
        );
        return false;
      }
    }
  }

  setLocationListener() async {
    final listener = l.Location.instance.onLocationChanged;
    listener.listen((l.LocationData currentLocation) {
      if(mounted) {
        //GeoPoint
        GeoPoint geoPoint = GeoPoint(
            currentLocation.latitude, currentLocation.longitude);

        //Add Marker on Map
        addMarker(currentLocation);

        print("Bus Id: ${this.bus.id}");
        //Firebase Update
        FirebaseFirestore.instance
            .collection("buses")
            .doc(this.bus.id)
            .update({
          "currentLocation": geoPoint
        });
      }
    });
  }

  addMarker(l.LocationData loc) {
    final _id = UniqueKey();
    final _marker = Marker(
        markerId: MarkerId(_id.toString()),
        draggable: false,
        infoWindow: InfoWindow(title: 'You are here', snippet: ''),
        position: LatLng(loc.latitude, loc.longitude)
    );
    setState(() {
      this.allMarkers.add(_marker);
    });
    moveCamera(loc);
  }

  moveCamera(l.LocationData _loc) {
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(
            _loc.latitude,
            _loc.longitude
        ),
        zoom: 13,
      ),
    ),);
  }

}
