import 'package:back_pressed/back_pressed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zoe/Driver_pages/select%20your%20bus.dart';
import 'package:zoe/model/buss.dart';
import 'package:location/location.dart' as loc;
import 'package:zoe/notifications.dart';

import '../auth/LoginIn.dart';
import '../changePassword.dart';
import '../model/user.dart';

class DriverHome extends StatefulWidget {
  MyUser currentUser;
  Bus bus;

  DriverHome({Key key, this.currentUser}) : super(key: key);

  @override
  _DriverHomeState createState() => _DriverHomeState();
}

class _DriverHomeState extends State<DriverHome> {
  String filename = "newSchedule";
  String scheduleFileUrl = '';
  MyUser currentUser;
  var _location = loc.Location();
  loc.PermissionStatus _status;
  bool isServiceEnabled = false;
  List<Marker> markers = [];
  GoogleMapController _mapController;
  String adresses = 'your adress here';

  @override
  initState() {
    super.initState();
    _initialize();
  }

  _initialize() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getUserDetails();
      _location.hasPermission().then(_updateStatus);
      getScheduleFile();
    });
  }

  getScheduleFile() async {
    final ref = await FirebaseStorage.instance
        .ref('schedule')
        .list(ListOptions(maxResults: 1));
    if (ref != null && ref.items.length > 0) {
      final firstRef = ref.items.first;
      final downloadUrl = await firstRef.getDownloadURL();
      print("File path");
      print(downloadUrl);
      setState(() {
        this.scheduleFileUrl = downloadUrl;
      });
    }
    //await launch(scheduleFileUrl);
  }

  _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    setState(() {
      this.currentUser = MyUser.fromJson(data);
    });
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LogIn()), (route) => false);
  }

  Widget build(BuildContext context) {
    return OnBackPressed(
      perform: () {
        SystemNavigator.pop();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(adresses),
          //actions: [
          //IconButton(
          //icon: Icon(Icons.search),
          //onPressed: () {
          //_logout();
          //},
          //)
          //],
        ),
        body: Stack(
          children: [
            GoogleMap(
              initialCameraPosition:
                  CameraPosition(target: LatLng(40.7, -74), zoom: 13),
              onTap: (position) {
                print('tapped on $position');
                _getAddress(position);
              },
              markers: Set.from(this.markers),
              onMapCreated: (GoogleMapController controller) {
                _mapController = controller;
              },
            ),
            //Align(alignment: Alignment.topCenter,
            //child: Container(
            //height: 40,
            //width: MediaQuery
            //.of(context)
            //.size
            //  .width,
            //color: Colors.white,
            //padding: EdgeInsets.all(10),
            //margin: EdgeInsets.only(top: 2, left: 5, right: 5),
            //child: Text(adresses),
            //),)
          ],
        ),
        drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Color(
                0xff2b2a2a), //This will change the drawer background to blue.
            //other styles
            //decoration: BoxDecoration(color: Color(0xff2b2a2a)),
          ),
          //color: Colors.white,
          //width: 255,
          child: Drawer(
            child: ListView(
              children: [
                Container(
                  // decoration: BoxDecoration(color: Color(0xff2b2a2a)),
                  height: 125,
                  child: DrawerHeader(
                    decoration: BoxDecoration(color: Color(0xff2b2a2a)),
                    child: Row(
                      children: [
                        Image.asset("assets/imge/kindpng.png",
                            height: 65, width: 64),
                        SizedBox(width: 16),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome',
                              style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                  fontFamily: "Brand-Bold"),
                            ),
                            SizedBox(height: 6),
                            Text(
                              ' ${this.currentUser?.name ?? ''}',
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.white,
                                  fontFamily: "Brand-Bold"),
                            ),
                            //Text("visit profile",style: TextStyle(fontSize: 12.0 , color: Colors.white),),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.black,
                  thickness: 1,
                ),
                //DividerWidget(),
                SizedBox(height: 12.0),
                ListTile(
                  leading:
                      Icon(Icons.directions_bus_sharp, color: Colors.white),
                  onTap: () {
                    //Have to pass currentUser's value here
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SelectYourbus(
                              currentUser: this.currentUser,
                            )));
                  },
                  title: Text(
                    "choose yours bus",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.add_location, color: Colors.white),
                  onTap: () {
                    //Have to pass currentUser's value here
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DriverHome()));
                  },
                  title: Text(
                    "my location",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.schedule, color: Colors.white),
                  onTap: () async {
                    //Have to pass currentUser's value here
                    await launch(scheduleFileUrl);
                  },
                  title: Text(
                    "Bus Schedule",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.notification_important_sharp,
                      color: Colors.white),
                  onTap: () {
                    //Have to pass currentUser's value here
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => NotificationsPage()));
                  },
                  title: Text(
                    "Notifications",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),

                ListTile(
                  leading: Icon(Icons.security, color: Colors.white),
                  onTap: () {
                    //Have to pass currentUser's value here
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ChangePassword()));
                  },
                  title: Text(
                    "Change Password",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.logout, color: Colors.white),
                  onTap: () {
                    _logout();
                  },
                  title: Text(
                    "Logout",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),

                ListTile(
                  leading: Icon(Icons.close, color: Colors.white),
                  onTap: () {
                    //Have to pass currentUser's value here
                    Navigator.pop(context, true);
                  },
                  title: Text(
                    "close",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.my_location_outlined),
          onPressed: () {
            getCurrentLocation(askPermission: true);
          },
        ),
      ),
    );
  }

  void _updateStatus(loc.PermissionStatus _status) {
    if (_status != loc.PermissionStatus.granted) {
      isServiceEnabled = false;
      //TODO Ask for permision
      _askPermission();
    } else {
      setState(() {
        this._status = _status;
      });
      _requestLocationServices();
    }
  }

  void _askPermission() {
    if (_status == null) {
      _location.requestPermission().then((status) {
        if (status == loc.PermissionStatus.granted) {
          setState(() {
            this._status = status;
            //other work here
          });
        }
        //check for location Services Status
        _requestLocationServices();
      });
    }
  }

  void _requestLocationServices() async {
    if (await _location.serviceEnabled()) {
      isServiceEnabled = true;
      //todo:get users current location
      getCurrentLocation();
    } else {
      isServiceEnabled = true;
      var enabled = await _location.requestService();
      if (enabled) {
        //todo: get user current  location
        getCurrentLocation();
      }
    }
  }

  void getCurrentLocation({bool askPermission = false}) async {
    if (isServiceEnabled && this._status == loc.PermissionStatus.granted) {
      //get current location
      loc.LocationData locationData = await _location.getLocation();
      LatLng position =
          LatLng(locationData.latitude ?? 40.7, locationData.longitude ?? -74);
      //todo: get user Address here
      _getAddress(position);
    } else if (askPermission) {
      _askPermission();
    }
  }

  void _getAddress(LatLng position) async {
    final coordinates = Coordinates(position.latitude, position.longitude);
    //TODO get location address
    final addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      this.adresses = addresses.first.featureName +
          " " +
          addresses.first.subAdminArea +
          " " +
          addresses.first.countryName;
    });

    //TODO add marker
    _addNewMarker(position, adresses);
  }

  void _addNewMarker(LatLng position, String adresses) {
    Marker _newMarker = Marker(
        markerId: MarkerId('currentUser'),
        position: position,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
        infoWindow: InfoWindow(
            title: 'you are here', snippet: adresses ?? 'your location'));
    setState(() {
      this.markers.clear();
      this.markers.add(_newMarker);
    });
    if (_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newLatLng(position));
    }
  }
}
