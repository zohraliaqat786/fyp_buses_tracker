import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/Driver_pages/select%20your%20bus.dart';
import 'package:zoe/auth/LoginIn.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';
import 'package:location/location.dart'as loc;

import 'Driver_home.dart';
class MyLocation extends StatefulWidget {
  MyUser currentUser;
  Bus bus;
   MyLocation({Key key}) : super(key: key);

  @override
  _MyLocationState createState() => _MyLocationState();
}

class _MyLocationState extends State<MyLocation> {

  MyUser currentUser;
  var _location = loc.Location();
  loc.PermissionStatus _status;
  bool isServiceEnabled = false;
  List<Marker>markers = [];
  GoogleMapController _mapController;
  String adresses = 'your adress here';
  @override
  initState() {
    _getUserDetails();
    super.initState();
    _location.hasPermission().then(_updateStatus);
  }

  _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    setState(() {
      this.currentUser = MyUser.fromJson(data);
    });
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LogIn()), (route) => false);
  }
  @override
  Widget build(BuildContext context) {
    child: Scaffold(
        appBar: AppBar(
        title: Text(adresses),
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
    ),
    body: Stack(
    children: [
    GoogleMap(initialCameraPosition: CameraPosition(
    target: LatLng(40.7, -74), zoom: 13),
    onTap: (position) {
    print('tapped on $position');
    _getAddress(position);
    },
    markers: Set.from(this.markers),
    onMapCreated: (GoogleMapController controller) {
    _mapController = controller;
    },),
    Align(alignment: Alignment.topCenter,
    child: Container(
    height: 40,
    width: MediaQuery
        .of(context)
        .size
        .width,
    color: Colors.white,
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.only(top: 2, left: 5, right: 5),
    child: Text(adresses),
    ),)
    ],
    ),

      drawer: Container(
      color: Colors.white,
      width: 255,
      child: Drawer(
        child: ListView(
          children: [
            Container(
              height: 105,
              child: DrawerHeader(
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  children: [
                    Image.asset("assets/imge/user.png",height: 65,width: 64),
                    SizedBox(width: 16),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Welcome  driver ${this.currentUser.name ?? ''}',style: TextStyle(fontSize: 12.0 , color: Colors.black,fontFamily: "Brand-Bold"),),
                        SizedBox(height: 6),
                        Text("visit profile",style: TextStyle(fontSize: 12.0 , color: Colors.black),),

                      ],
                    )
                  ],
                ),
              ),
            ),
            Divider(),
            //DividerWidget(),
            SizedBox(height: 12.0),
            ListTile(
              leading: Icon(Icons.directions_bus_sharp),
              onTap: () {
                //Have to pass currentUser's value here
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) =>
                        SelectYourbus(currentUser: this.currentUser,)));
              },
              title: Text("choose yours bus",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),
            ListTile(
              leading: Icon(Icons.add_location),
              onTap: () {
                //Have to pass currentUser's value here
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) =>
                        DriverHome()));
              },
              title: Text("my location",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),
            ListTile(
              leading: Icon(Icons.schedule),
              title: Text("Bus Schedule",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),
            ListTile(
              leading: Icon(Icons.notification_important_sharp),
              title: Text("Notifications",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),
            ListTile(
              leading: Icon(Icons.logout),
              onTap: (){
                _logout();},
              title: Text("Logout",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),
            ListTile(
              leading: Icon(Icons.close),
              onTap: () {
                //Have to pass currentUser's value here
                Navigator.pop(context,true);
              },
              title: Text("chose",style: TextStyle(fontSize: 15,color: Colors.black),),
            ),

          ],
        ),
      ),

    ),
    floatingActionButton: FloatingActionButton(
    child: Icon(Icons.my_location_outlined),
    onPressed: () {
    getCurrentLocation();
    },
    ),
    );
  }
  void _updateStatus(loc.PermissionStatus _status) {
    if (_status != loc.PermissionStatus.granted) {
      isServiceEnabled = false;
      //TODO Ask for permision
      _askPermission();
    } else {
      setState(() {
        this._status = _status;
      });
      _requestLocationServices();
    }
  }

  void _askPermission() {
    if (_status == null) {
      _location.requestPermission().then((status) {
        if (status == loc.PermissionStatus.granted) {
          setState(() {
            this._status = _status;
            //other work here
          });
        }
        //check for location Services Status
        _requestLocationServices();
      });
    }
  }

  void _requestLocationServices() async {
    if (await _location.serviceEnabled()) {
      isServiceEnabled = true;
      //todo:get users current location
      getCurrentLocation();
    }
    else {
      isServiceEnabled = true;
      var enabled = await _location.requestService();
      if (enabled) {
        //todo: get user current  location
        getCurrentLocation();
      }
    }
  }

  void getCurrentLocation() async {
    if (isServiceEnabled && this._status == loc.PermissionStatus.granted) {
      //get current location
      loc.LocationData locationData = await _location.getLocation();
      LatLng position = LatLng(
          locationData.latitude ?? 40.7, locationData.longitude ?? -74);
      //todo: get user Address here
      _getAddress(position);
    }
  }

  void _getAddress(LatLng position) async {
    final coordinates = Coordinates(position.latitude, position.longitude);
    //TODO get location address
    final addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    setState(() {
      this.adresses =
          addresses.first.featureName + " " + addresses.first.subAdminArea + " " + addresses.first.countryName;
    });

    //TODO add marker
    _addNewMarker(position,adresses);

  }
  void _addNewMarker(LatLng position,String adresses){
    Marker _newMarker = Marker(markerId: MarkerId('currentUser'),
        position: position,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
        infoWindow: InfoWindow(
            title: 'you are here',
            snippet: adresses ?? 'your location'
        )
    );
    setState(() {
      this.markers.clear();
      this.markers.add(_newMarker);
    });
    if (_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newLatLng(position));
    }

  }
}
