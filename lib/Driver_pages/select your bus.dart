import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/Driver_pages/Driver_home.dart';
import 'package:zoe/auth/LoginIn.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';

import 'buss Detail page.dart';
class SelectYourbus extends StatefulWidget {
  MyUser currentUser;
  Bus bus;
   SelectYourbus({Key key, this.currentUser}) : super(key: key);

  @override
  _SelectYourbusState createState() => _SelectYourbusState();
}

class _SelectYourbusState extends State<SelectYourbus> {
  MyUser currentUser;
  @override
  initState() {
    this.currentUser = widget.currentUser;
    super.initState();
    //subscribeNotificationsTopic();
  }

  Future<List<Bus>> _getMyBuses() async {
    List<Bus> _buses = [];
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference buses = firestore.collection('buses');
    final ref = buses.where('driverId', isEqualTo: this.currentUser.id);
    final snapshot = await ref.get();
    if (snapshot.docs.isNotEmpty) {
      final docs = snapshot.docs;
      docs.forEach((docSnap) {
        final _bus = Bus.fromJson(docSnap.data());
        _buses.add(_bus);
      });
    }
    return _buses;
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LogIn()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;

    return Scaffold(
        appBar: AppBar(
          title: Text('Select Your Bus'),
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        //body
        body: SingleChildScrollView(
          child: Column(
            children: [

              SizedBox(height: 15,width: 15,),


              FutureBuilder(
                future: this._getMyBuses(),
                builder: (context, AsyncSnapshot<List<Bus>> _snap) {

                  final _data = _snap.data;

                  return Column(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //Padding(padding:EdgeInsets.symmetric(horizontal: 12)),
                      _data?.isEmpty ?? false
                          ? SizedBox(width: 15,)
                          : Text(
                        'Buses',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 15
                        ),
                      ),
                      SizedBox(height: 15,),
                      _data?.isEmpty ?? false
                          ? SizedBox()
                          : ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: _data?.length ?? 0,
                        itemBuilder: (context, index) {
                          return ListTile(
                            onTap: (){
                              //TODO: Navigate to next Page i.e. Bus Details
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) => BusDetailPage(
                                        currentUser: this.currentUser,
                                        bus: _data[index],
                                      )
                                  )
                              );
                            },
                            title: Text('Bus No. (${_data[index].busNo})', style: textTheme.bodyText1,),
                            leading: Icon(CupertinoIcons.car_detailed, color: Colors.black87,),
                            trailing: IconButton(
                              onPressed: (){
                                //TODO: Navigate to next Page i.e. Bus Details
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => BusDetailPage(
                                          currentUser: this.currentUser,
                                          bus: _data[index],
                                        )
                                    )
                                );
                              },
                              icon: Icon(CupertinoIcons.chevron_forward, color: Colors.black87,),
                            ),
                          );
                        },
                      ),
                    ],
                  );
                },
              ),


            ],

          ),
        )

    );
  }
}
