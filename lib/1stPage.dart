import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/Admin_pages/Admin_SplashScreen.dart';
import 'auth/splashScrean.dart';
class extra extends StatefulWidget {
  const extra({Key key}) : super(key: key);

  @override
  _extraState createState() => _extraState();
}

class _extraState extends State<extra> {
  double height,width;

  @override
  Widget build(BuildContext context) {
    width= MediaQuery.of(context).size.width;
    height= MediaQuery.of(context).size.height;
    return Scaffold(
      body:SingleChildScrollView(
        child:Container(
          child:Column(
            children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            height: height* .3,
            //width: MediaQuery.of(context).size.width -40,
            //margin: EdgeInsets.symmetric(horizontal: 0),
            decoration: BoxDecoration(
             // color: Color(0xff055052),

                boxShadow: [BoxShadow(color: Colors.black87.withOpacity(0.39)),
                ],

                borderRadius: BorderRadius.circular(25),
              color: Color(0xff2b2a2a),

            ),
            child: Column(
              children: [
                SizedBox(
                  height: 120,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Text("Welcome",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                          ),
                        ),
                        //Ink.image(image:NetworkImage('assets/imge/admin.png')),
                        SizedBox(
                          height: 9,
                        ),
                        Text("FUUAST Bus Tracking   ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),)
                      ],
                    ),
                  ],

                ),
              ],
            ),
          ),

              Column(
                children: [

                  Column(
                    children: [
                      //SizedBox(height: 200,),
                      Image.asset('assets/imge/kindpng.png',height: 250,width: 250,)

                    ],
                  ),
              Container(

                padding:EdgeInsets.symmetric(horizontal: 70,vertical: 0),
                child: Column(
                  children: [
                    SizedBox(height: 20,),
                  MaterialButton(
                    //padding: EdgeInsets.symmetric(horizontal: 70,vertical: 70),
                    minWidth: double.infinity,
                    height: 60,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder:(context)=>Admin_SplashScreen()));
                    },
                    color: Color(0xff2b2a2a),

                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Colors.black
                        ),
                        borderRadius: BorderRadius.circular(30)
                    ),

                          child:Text("Admin",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 25
                            ),)
                           // Image.asset("assets/imge/admin.png",height: 40,width: 40,),


                  )
                  ],
                       )
                    ),
                ],
              ),


              Column(
                children: [
                  Container(
                      padding:EdgeInsets.symmetric(horizontal: 70,vertical: 20),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 0,
                        ),
                        MaterialButton(
                          //padding: EdgeInsets.symmetric(horizontal: 70,vertical: 70),
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>SlpashScrean()));
                          },
                          color: Color(0xff2b2a2a),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.black
                            ),
                            borderRadius: BorderRadius.circular(30),

                          ),
                          child: Text("User",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 25
                            ),

                          )
                          ,),
                      ],
                    ),
                    //margin: EdgeInsets.only(top:40),
                  ),
                ],
              )
            ],
          )

        ),

      ),

    );
  }
}
