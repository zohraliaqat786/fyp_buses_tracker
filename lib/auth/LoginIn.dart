import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/Admin_pages/Admin_home.dart';
import 'package:zoe/Driver_pages/Driver_home.dart';
import 'package:zoe/Rider_pages/RiverHome.dart';
import 'package:zoe/Rider_pages/addmystop.dart';
import 'package:zoe/auth/forgetPassword.dart';

import '../1stPage.dart';
import 'SignUp.dart';
import '../model/user.dart';
class LogIn extends StatefulWidget {
  const LogIn({Key key}) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  bool isLoading = false;
  MyUser user = MyUser();
  GlobalKey<FormState> formKey = GlobalKey();


  _validateForm() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      ///Login user here
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(
            email: this.user.email.toString(),
            password: this.user.password.toString()
        );
        setState(() {
          isLoading = false;
        });

        if (userCredential != null) {
          final user = await _getUserDetails();
          bool isVerified = userCredential.user.emailVerified;
          if (isVerified) {
            //Check if user is not admin
            if(user.type == null && user.type != 'admin') {
              if(user.isActive) {
                //Check if user is Driver or Rider
                if (user.isDriver) {
                  //TODO: Navigate to driver module
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) =>
                          DriverHome(currentUser: user,)));
                } else {
                  //TODO: Navigate to Rider Modules
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => RiverHome(currentUser: user,)));
                }
              } else {
                await FirebaseAuth.instance.signOut();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Yor account hasn't bee approved by admin yet")));
              }
            } else {
              await FirebaseAuth.instance.signOut();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("You are not allowed to Login to this app")));
            }

            //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => welcome()));
          } else {
            await userCredential.user.sendEmailVerification();
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text("Check your email for verification")));
            this.formKey.currentState.reset();
          }
        }
      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'user-not-found') {
          print('No user found for that email.');
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("No user found for that email.")));
        } else if (e.code == 'wrong-password') {
          setState(() {
            isLoading = false;
          });
          print('Wrong password provided for that user.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Wrong password provided for that user.")));
        }
      }
    }
  }
  Future<MyUser> _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    return MyUser.fromJson(data);
  }


  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (ctx) => extra()
            )),
          ),
        ),
        backgroundColor: Color(0xff2b2a2a),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30,vertical: 50),//distance btw login and upper edge
                child: Form(
                    key: this.formKey,
                    child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          //Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
                          Text("Login",style: TextStyle(color: Colors.white,fontSize: 43,fontWeight: FontWeight.bold),),
                          SizedBox(height: 20,),
                          Container(
                              padding: EdgeInsets.symmetric(horizontal: 40,vertical: 30),//container size

                                decoration: BoxDecoration(
                                 // color: Color(0xff055052),

                                  boxShadow: [BoxShadow(color: Colors.black87.withOpacity(0.49)),
                                        ],

                             borderRadius: BorderRadius.circular(15),
                           color: Color(0xffffffff),

                           ),
                            //Icon(CupertinoIcons.star, color: Colors.black,
                            //size: 55,),
                            child:Column(
                              children:[
                            // Text(" Login",style: TextStyle(color: Colors.white,fontSize: 43,fontWeight: FontWeight.bold),),
                              SizedBox(height: 30,),
                             TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) =>
                              (value != null && value.contains("@") &&
                                value.contains("."))
                                ? null
                                : "Email must be valid",
                                style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1,
                            decoration: InputDecoration(
                                hintText: "Email",
                              hintStyle: Theme.of(context).textTheme.bodyText1,
                                //hintStyle: TextStyle(color: Colors.black87 ),

                                border: new OutlineInputBorder(
                                  borderSide: new BorderSide(
                                    color: Colors.black,
                                  ),
                                ),

                                //border: InputBorder.none
                            ),
                            onSaved: (value) {
                              user.email = value;
                            },
                          ),
                          SizedBox(height: 10,),

                          TextFormField(
                            obscureText: true,
                            validator: (value) =>
                            (value != null && value.length >= 6)
                                ? null
                                : "Password must contain at least 6 chars",
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1,
                            decoration: InputDecoration(
                                hintText: "Password",
                                //hintStyle: TextStyle(color: Colors.black87 ),

                              border: new OutlineInputBorder(
                                borderSide: new BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                                hintStyle: Theme.of(context).textTheme.bodyText1,
                                //border: InputBorder.none
                            ),
                            onSaved: (value) {
                              user.password = value;
                            },
                          ),
                          SizedBox(height: 40,),

                          this.isLoading
                          ? SizedBox(height: 35, child: CircularProgressIndicator())
                          :ElevatedButton(
                            onPressed: () {
                              _validateForm();
                            },
                            style: ElevatedButton.styleFrom(
                              primary:Color(0xff2b2a2a),
                                shape: StadiumBorder(),shadowColor: Colors.black),
                            child: Center(
                              child: Text("Login",),
                            ),

                          ),

                          SizedBox(height: 10,),

                          TextButton(
                              onPressed: () {
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) =>
                                    SignUp()));
                          },
                              child: Center(
                                child: Text("Signup instead",

                                  style: TextStyle(
                                    color: Colors.black87,

                                  ),),
                              )),

                          TextButton(onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ForgetPassword()));
                          },
                              child: Center(
                                child: Text("Forgot Password",

                                  style: TextStyle(
                                    color: Colors.black87,
                                  ),),
                              )),

                        ]
                    )

                )
                    ]
                   ) ,

                ),
            ),
        ),
      );
    }
  }


