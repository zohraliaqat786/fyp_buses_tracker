import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/auth/LoginIn.dart';

import '../database.dart';
import '../model/user.dart';

class SignUp extends StatefulWidget {
  //const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isLoading = false;
  MyUser user = MyUser();
  GlobalKey<FormState> formKey = GlobalKey();

  _validateForm() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: this.user.email.toString(),
                password: this.user.password.toString());

        await userCredential.user.updateDisplayName(this.user.name);

        await userCredential.user.sendEmailVerification();

        print("User id is: " + userCredential.user.uid);

        ///Add user to Firestore
        await addUserToFirestore(userCredential.user, this.user.name);

        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Check your email for verification")));

        setState(() {
          isLoading = false;
        });

        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => LogIn()));
      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'weak-password') {
          print('The password provided is too weak.');
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("The password provided is too weak.")));
        } else if (e.code == 'email-already-in-use') {
          print('The account already exists for that email.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("The account already exists for that email.")));
        }
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Error registering new account")));
      }
    }
  }

  Future<void> addUserToFirestore(User _user, String _updatedName) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    // Create a CollectionReference called users that references the firestore collection
    CollectionReference users = firestore.collection('users');
    this.user.email = _user.email.toLowerCase(); // Stokes and Sons
    this.user.id = _user.uid;
    this.user.isActive = false;

    ///For manual Doc Id
    users
        .doc(_user.uid)
        .set(this.user.toMap())
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      backgroundColor: Color(0xff2b2a2a),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
          child: Form(
            //Have uncommented this line
            key: this.formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Signup",style: TextStyle(color: Colors.white,fontSize: 43,fontWeight: FontWeight.bold),),
                //Icon(
                  //CupertinoIcons.star,
                  //color: Colors.white,
                  //size: 55,
                //),
                SizedBox(
                  height: 20,
                ),
                Container(

                  padding: EdgeInsets.symmetric(horizontal: 40,vertical: 30),

                  decoration: BoxDecoration(
                    // color: Color(0xff055052),

                    boxShadow: [BoxShadow(color: Colors.black87.withOpacity(0.49)),
                    ],

                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xffffffff),

                  ),
                child:Column(
                  children:[
                    SizedBox(height: 20,),
                TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (value) => (value != null && value.length > 1)
                      ? null
                      : "Name can't be empty",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Name",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      //border: InputBorder.none
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(
                          color: Colors.black,
                        ),)
                       ),
                  onSaved: (value) {
                    user.name = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) => (value != null &&
                          value.contains("@") &&
                          value.contains("."))
                      ? null
                      : "Email must be valid",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                     // border: InputBorder.none
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Colors.black,
                      ),)
                  ),

                  onSaved: (value) {
                    user.email = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  obscureText: true,
                  validator: (value) => (value != null && value.length >= 6)
                      ? null
                      : "Password must contain at least 6 chars",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      //border: InputBorder.none
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(
                          color: Colors.black,
                        ),)),
                  onSaved: (value) {
                    user.password = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      "Rider",
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    Switch(
                        value: this.user.isDriver,
                        onChanged: (value) {
                          setState(() {
                            this.user.isDriver = value;
                          });
                        }),
                    Text(
                      "Driver",
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return this.user.isDriver
                          ? "CNIC can't be empty"
                          : "MIS id can't be empty";
                    } else {
                      if (this.user.isDriver) {
                        if (value.length != 13) {
                          return "CNIC should be 13 digits long";
                        }
                      } else {
                        if (value.length != 4) {
                          return "MIS id should be at least 4 digits long";
                        }
                      }
                    }
                    return null;
                  },
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: this.user.isDriver ? "CNIC" : "MIS id",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      //border: InputBorder.none
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(
                          color: Colors.black,
                        ),)),
                  onSaved: (value) {
                    if (this.user.isDriver) {
                      user.CNIC = value;
                    } else {
                      user.MISid = value;
                    }
                  },
                ),
                this.isLoading
                    ? SizedBox(height: 35, child: CircularProgressIndicator())
                    : ElevatedButton(
                        onPressed: () {
                          _validateForm();
                        },
                        style: ElevatedButton.styleFrom(
                             primary:Color(0xff2b2a2a),
                            shape: StadiumBorder()),
                        child: Center(
                          child: Text("Register"),
                        ),
                      ),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Center(
                      child: Text(
                        "Back to Login",
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                    ))
              ],
            ),
          ),
            ]
        ),
        )
      ),
      ),
    );
  }
}
