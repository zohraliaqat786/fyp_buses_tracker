import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:zoe/main%20splashScreen.dart';
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Zoe',
      theme: ThemeData(
          scaffoldBackgroundColor: Color(0xFFFDFDFD),
          accentColor: Color(0xff2b2a2a),
          primaryColor: Color(0xff2b2a2a),
          textTheme: TextTheme(
            subtitle1: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
            subtitle2: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
            bodyText1: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.black),
            bodyText2: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: Colors.black),
            caption: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: Colors.white),
          )
      ),
      home:MainSplashScreen(),

    );
}
}




