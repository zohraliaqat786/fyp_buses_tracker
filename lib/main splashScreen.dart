import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/1stPage.dart';
import 'package:zoe/Admin_pages/Admin_home.dart';

import 'Driver_pages/Driver_home.dart';
import 'Rider_pages/RiverHome.dart';
import 'auth/LoginIn.dart';
import 'auth/splashScrean.dart';
import 'model/user.dart';
class MainSplashScreen extends StatefulWidget {
  const MainSplashScreen({Key key}) : super(key: key);

  @override
  _MainSplashScreenState createState() => _MainSplashScreenState();
}

class _MainSplashScreenState extends State<MainSplashScreen> {
  var A='admin';

  initState() {
    super.initState();
    loadData();
    //subscribeNotificationsTopic();
  }
  void loadData() async {
    Future.delayed(Duration(milliseconds: 700), () async {
      ///Get value and navigate
      final _dbUser = FirebaseAuth.instance.currentUser;
      bool isSignedIn = _dbUser != null;
      if(isSignedIn) {
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        CollectionReference users = firestore.collection('users');
        final _userDetails = await users.doc(_dbUser.uid).get();
        Map data = _userDetails.data();
        final _user =  MyUser.fromJson(data);
        //Check if user is Driver or Rider
        if(_user.type != null && _user.type == 'admin') {
          //TODO: Navigate to driver module
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => AdminHome(currentUser: _user,)));
        }
        else if(_user.isDriver) {
          //TODO: Navigate to Rider Modules
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => DriverHome(currentUser: _user,)));
        } else {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => RiverHome(currentUser: _user,)));
        }
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => extra()));
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CupertinoIcons.settings, color: Colors.black,),

            SizedBox(height: 20),


            SizedBox(
              height: 35,
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );
  }
}
