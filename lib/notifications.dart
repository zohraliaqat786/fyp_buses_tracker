import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:zoe/model/notification.dart';

class NotificationsPage extends StatefulWidget {
  final bool isDriver;
  final bool isRider;
  NotificationsPage({Key key, 
    this.isDriver = false,
    this.isRider = false,
  }) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  bool isLoading = false;
  List<MyNotification> notifications = [];

  @override
  void initState() {
    super.initState();
    _getNotifications();
  }

  Future<void> _getNotifications() async {
    notifications.clear();
    setState((){isLoading = true;});
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference notificationsRef = firestore.collection('notifications');
    
    var query = notificationsRef
        .where('forDriver', isEqualTo: widget.isDriver);
    if(widget.isRider) {
      query = notificationsRef
          .where('forRider', isEqualTo: widget.isRider);
    }

    query.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _notif = MyNotification.fromJson(docSnap.data());
          setState(() {
            notifications.add(_notif);
            isLoading = false;
          });
        });
      }
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(),
              this.isLoading
              ? CircularProgressIndicator()
                  : this.notifications.isEmpty
              ? _buildNotificationWidget()
                  : ListView.builder(
                shrinkWrap: true, primary: false,
                itemCount: this.notifications.length,
                itemBuilder: (context, i) {
                  MyNotification _notif = this.notifications[i];
                  DateTime _time = DateTime.fromMillisecondsSinceEpoch(_notif.time);
                  DateFormat _format = DateFormat('dd/MM/yyyy hh:mm a');
                  final _dateStr = _format.format(_time);
                  return ListTile(
                    leading: Icon(CupertinoIcons.bell_circle_fill, color: Colors.black, size: 38,),
                    title: Text(_notif.title, style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      color: Colors.black
                    )),
                    subtitle: Text('$_dateStr\n${_notif.body}', style: Theme.of(context).textTheme.bodyText2?.copyWith(
                      color: Colors.grey
                    )),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNotificationWidget() {
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [

          ClipRRect(
            borderRadius: BorderRadius.circular(1000),
            child: Container(
              height: 100,
              width: 100,
              color: Colors.grey,
              child: Center(
                child: Icon(
                  CupertinoIcons.bell_slash_fill,
                  size: 38,
                  color: Colors.black,
                ),
              ),
            ),
          ),

          SizedBox(height: 10,),
          Text("No notification found for you!",
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                color: Colors.grey
            ),
          )

        ],
      ),
    );
  }
}
