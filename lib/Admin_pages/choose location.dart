import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/model/BusRoute.dart';
import 'package:location/location.dart' as l;
class ChooseLocation extends StatefulWidget {
  const ChooseLocation({Key key}) : super(key: key);

  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  LocationModel _locationModel;

  List<Marker> allMarkers = [];
  GoogleMapController _mapController;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: [

          Container(
            width: size.width,
            height: size.height,
            // color: Colors.indigo,
            child: GoogleMap(
              onMapCreated: (GoogleMapController _controller) {
                this._mapController = _controller;
                // setMapStyle(mapStyle);
              },
              initialCameraPosition: CameraPosition(
                  target: LatLng(33.6780513, 73.1843443), zoom: 13
              ),
              onTap: (position){
                //TODO: This is the Route's actual location
                l.LocationData locData = l.LocationData.fromMap({
                  'latitude': position.latitude,
                  'longitude': position.longitude
                });
                addMarker(locData);
              },
              mapType: MapType.normal,
              markers: Set.from(this.allMarkers),
            ),
          ),

          _locationModel == null
              ? SizedBox()
              :
          Positioned(
              bottom: 10,
              right: 10,
              left: 10,
              child: ElevatedButton(
                onPressed: () {
                  // _validateForm();
                  Navigator.of(context).pop(_locationModel);
                },
                style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),
                child: Center(
                  child: Text("Select this location"),
                ),
              )
          ),



        ],
      ),
    );
  }

  getCurrentLocation() async {
    l.LocationData locationData = await l.Location.instance.getLocation();
    addMarker(locationData);
  }


  addMarker(l.LocationData loc) {

    final _id = UniqueKey();
    final _marker = Marker(
        markerId: MarkerId(_id.toString()),
        draggable: false,
        infoWindow: InfoWindow(title: 'You are here', snippet: ''),
        position: LatLng(loc.latitude, loc.longitude)
    );
    setState(() {
      _locationModel = LocationModel(
          latitude: loc.latitude,
          longitude: loc.longitude
      );
      this.allMarkers.clear();
      this.allMarkers.add(_marker);
    });
    moveCamera(loc);
  }

  moveCamera(l.LocationData _loc) {
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(
            _loc.latitude,
            _loc.longitude
        ),
        zoom: 13,
      ),
    ),);


  }
}
