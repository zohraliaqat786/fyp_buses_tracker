import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:zoe/Admin_pages/repository.dart';
import 'package:zoe/model/notification.dart';

import 'global Variables.dart';
class SendNotificationWidget extends StatefulWidget {
  const SendNotificationWidget({Key key}) : super(key: key);

  @override
  _SendNotificationWidgetState createState() => _SendNotificationWidgetState();
}

class _SendNotificationWidgetState extends State<SendNotificationWidget> {
  GlobalKey<FormState> formKey = GlobalKey();
  String title = '';
  String body = '';
  bool sendToRider = false;
  bool sendToDriver = false;
  bool isLoading = false;

  _validateForm() async {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();

      if(sendToRider || sendToDriver) {
        // Send Notification with respective topics
        setState((){
          this.isLoading = true;
        });
        if(sendToDriver) {
          await notifyUser(GlobalVariables.driverNotificationsTopic, title, body);
        }
        if(sendToRider) {
          await notifyUser(GlobalVariables.riderNotificationsTopic, title, body);
        }
        //TODO: Add notification to firestore
        MyNotification _notif = MyNotification(
          title: title, body: body, forDriver: sendToDriver, forRider: sendToRider,
          time: DateTime.now().millisecondsSinceEpoch
        );
        await _addNotification(_notif);
        setState((){
          this.isLoading = false;
        });
        formKey.currentState.reset();
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("Notification Sent"),
              behavior: SnackBarBehavior.floating,
            )
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("You must select either Rider or Driver or Both"),
              behavior: SnackBarBehavior.floating,
            )
        );
      }

    }
  }

  Future<void> _addNotification(MyNotification _notification) async {
    var uuid = Uuid();
    final _id = uuid.v1();
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    // Create a CollectionReference called buses that references the firestore collection
    CollectionReference buses = firestore.collection('notifications');
    _notification.id = _id;

    print(_notification.toMap());
    ///For manual Doc Id
    buses.doc(_notification.id).set(_notification.toMap())
        .then((value) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Notification sent")));
    })
        .catchError((error) {
      print("Failed to add notification: $error");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error adding Notification")));
    });

  }

  notifyUser(String topic, String title, String body) async {
    //TODO: Fire FCM
    await sendNotification(
        topic,
        title: title,
        notificationBody: body
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Notification"),
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            key: this.formKey,
            child: Column(
              children: [

                TextFormField(
                  validator: (value) => value != null && value.length > 1 ? null : "Title can't be empty",
                  style: Theme.of(context).textTheme.bodyText1,
                  onSaved: (value) {
                    title = value;
                  },
                  decoration: InputDecoration(
                    labelText: "Notification Title",
                    labelStyle: Theme.of(context).textTheme.bodyText1,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1),
                    ),
                    enabledBorder:  OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1),
                    ),
                  ),
                ),

                SizedBox(height: 15,),

                TextFormField(
                  validator: (value) => value != null && value.length > 1 ? null : "Message can't be empty",
                  style: Theme.of(context).textTheme.bodyText1,
                  onSaved: (value) {
                    body = value;
                  },
                  minLines: 3,
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: "Your Message",
                    labelStyle: Theme.of(context).textTheme.bodyText1,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1),
                    ),
                    enabledBorder:  OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1),
                    ),
                  ),
                ),

                SizedBox(height: 15,),

                Container(
                  child: Row(
                    children: [

                      Expanded(child: InkWell(
                        onTap: (){
                          setState((){
                            sendToRider = !sendToRider;
                          });
                        },
                        child: Row(
                          children: [
                            myCheckbox(sendToRider),
                            SizedBox(width: 15,),
                            Text("Riders", style: Theme.of(context).textTheme.subtitle2.copyWith(
                              color: Colors.black
                            ),)
                          ],
                        ),
                      )),

                      SizedBox(width: 15,),

                      Expanded(child: InkWell(
                        onTap: (){
                          setState((){
                            sendToDriver = !sendToDriver;
                          });
                        },
                        child: Row(
                          children: [
                            myCheckbox(sendToDriver),
                            SizedBox(width: 15,),
                            Text("Driver", style: Theme.of(context).textTheme.subtitle2.copyWith(
                              color: Colors.black
                            ),)
                          ],
                        ),
                      ))

                    ],
                  ),
                ),

                SizedBox(height: 15,),

                this.isLoading
                    ? SizedBox(height: 35, child: CircularProgressIndicator())
                    : ElevatedButton(
                  onPressed: () {
                    _validateForm();
                  },
                  style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),
                  child: Center(
                    child: Text("Send Notification"),
                  ),
                ),


              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget myCheckbox(bool isChecked) {
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          border: Border.all(color: Colors.green, width: 0.8),
          color: isChecked ? Colors.green : Colors.transparent
      ),
      child: Center(
        child: isChecked ? Icon(
          CupertinoIcons.checkmark_alt,
          color: Colors.white,
          size: 14,
        ) : SizedBox(),
      ),
    );
}
}
