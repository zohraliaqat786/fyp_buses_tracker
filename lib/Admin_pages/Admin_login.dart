import 'package:back_pressed/back_pressed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zoe/Admin_pages/Admin_home.dart';
import 'package:zoe/auth/SignUp.dart';
import 'package:zoe/auth/forgetPassword.dart';
import 'package:zoe/model/user.dart';

import '../1stPage.dart';
import '../HomePage.dart';
class Admin_login extends StatefulWidget {
  //const Admin_login({Key? key}) : super(key: key);

  @override
  _Admin_loginState createState() => _Admin_loginState();
}

class _Admin_loginState extends State<Admin_login> {
  bool isLoading = false;

  MyUser user = MyUser();

  GlobalKey<FormState> formKey = GlobalKey();

  _validateForm() async {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();
      ///Login user here
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: this.user.email.toString(),
            password: this.user.password.toString()
        );

        if(userCredential != null) {
          final user = await _getUserDetails();
          bool isVerified = userCredential.user.emailVerified;
          setState(() {
            isLoading = false;
          });
          if(isVerified) {
            ///If user is admin =>> Login
            ///Otherwise Log out the current user
            if(user.type != null && user.type == 'admin') {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => AdminHome()));
            } else {
              await FirebaseAuth.instance.signOut();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("You are not allowed to Login to this app")));
            }
          } else {
            await userCredential.user.sendEmailVerification();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Check your email for verification")));
            this.formKey.currentState.reset();
          }
        } else {
          setState(() {
            isLoading = false;
          });
        }

      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'user-not-found') {
          print('No user found for that email.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("No user found for that email.")));
        } else if (e.code == 'wrong-password') {
          setState(() {
            isLoading = false;
          });
          print('Wrong password provided for that user.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Wrong password provided for that user.")));
        }
      }
    }
  }
  Future<MyUser> _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    return MyUser.fromJson(data);
  }



  @override
  Widget build(BuildContext context) {

    return  Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded, color: Colors.white),
          onPressed: () => Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (ctx) => extra()
          )),
        ),
      ),
        //SystemNavigator.pop();
      backgroundColor: Color(0xff2b2a2a),
      body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Form(
                  key: this.formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(height: 70,),//distance from start to login
                        Text("Login",style: TextStyle(color: Colors.white,fontSize: 43,fontWeight: FontWeight.bold),),
                        SizedBox(height: 20,),//distance from login to container
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 40,vertical: 30),

                            decoration: BoxDecoration(
                              // color: Color(0xff055052),

                              boxShadow: [BoxShadow(color: Colors.black87.withOpacity(0.49)),
                              ],

                              borderRadius: BorderRadius.circular(15),
                              color: Color(0xffffffff),

                            ),
                            child:Column(
                            children:[

                             SizedBox(height: 30,),

                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) =>
                          (value != null && value.contains("@") &&
                              value.contains("."))
                              ? null
                              : "Email must be valid",
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1,
                          decoration: InputDecoration(
                              hintText: "Email",
                              hintStyle: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyText1,
                            border: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                color: Colors.black,
                              ),
                            ),
                             // border: InputBorder.none
                          ),
                          onSaved: (value) {
                            user.email = value;
                          },
                        ),
                        SizedBox(height: 10,),

                        TextFormField(
                          obscureText: true,
                          validator: (value) =>
                          (value != null && value.length >= 6)
                              ? null
                              : "Password must contain at least 6 chars",
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1,
                          decoration: InputDecoration(
                              hintText: "Password",
                              hintStyle: Theme.of(context).textTheme.bodyText1,
                            border: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                color: Colors.black,
                              ),
                            ),
                              //border: InputBorder.none
                          ),
                          onSaved: (value) {
                            user.password = value;
                          },
                        ),
                        SizedBox(height: 20,),

                        this.isLoading
                            ? SizedBox(height: 35, child: CircularProgressIndicator())
                            :ElevatedButton(
                          onPressed: () {
                            _validateForm();
                          },
                          style: ElevatedButton.styleFrom(
                              primary:Color(0xff2b2a2a),
                              shape: StadiumBorder(),shadowColor: Colors.black),
                          child: Center(
                            child: Text("Login",),
                          ),

                        ),

                        SizedBox(height: 10,),

                      //  TextButton(
                        //    onPressed: () {
                          //    Navigator.of(context).push(
                            //      MaterialPageRoute(builder: (context) =>
                              //        SignUp()));
                            //},
                            //child: Center(
                              //child: Text("Signup instead",

                                //style: TextStyle(
                                  //color: Colors.black,

                                //),),
                            //)),

                        TextButton(onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ForgetPassword()));
                        },
                            child: Center(
                              child: Text("Forgot Password",

                                style: TextStyle(
                                  color: Colors.black,
                                ),),
                            )),

                      ]
                  )
              )
              ]
      )
    )
          )
      ),
    );
  }
}
