import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:zoe/model/BusRoute.dart';

import 'choose location.dart';
class AddRoute extends StatefulWidget {
  //const AddRoute({Key? key}) : super(key: key);

  @override
  _AddRouteState createState() => _AddRouteState();
}

class _AddRouteState extends State<AddRoute> {
  GlobalKey<FormState> formKey = GlobalKey();
  var uuid = Uuid();
  BusRoute route = BusRoute();

  bool isLoading = false;

  @override
  initState(){
    super.initState();
  }

  _validateForm() {
    ///0 - Check if some location selected
    ///1 - Validate form
    ///2 - Generate ID
    ///3 - Add To Firestore

      //1
    if(this.route.location!=null) {
      if (this.formKey.currentState.validate()) {
        this.formKey.currentState.save();
        //2
        this.route.id = uuid.v1();
        setState(() {
          this.isLoading = true;
        });

        //3
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        // Create a CollectionReference called buses that references the firestore collection
        CollectionReference routes = firestore.collection('routes');

        Map _routeMap = this.route.toMap();
        /*
        GeoPoint geoPoint = GeoPoint(
            this.route.location.latitude, this.route.location.longitude);

        _routeMap['location'] = geoPoint;*/


        print(this.route.toMap());

        ///For manual Doc Id
        routes.doc(this.route.id).set(_routeMap)
            .then((value) {
          setState(() {
            this.isLoading = false;
          });
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Added New Route")));
          Navigator.of(context).pop();
        })
            .catchError((error) {
          setState(() {
            this.isLoading = false;
          });
          print("Failed to add route: $error");
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Error adding route")));
        });
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Please choose route location")));
    }


  }


  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text('Add new Route'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Form(
            key: this.formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 20,),

                  TextFormField(
                    keyboardType: TextInputType.text,
                    validator: (value) => value != null && value.length > 0 ? null : "Route name can't be empty",
                    style: Theme.of(context).textTheme.bodyText1,
                    decoration: InputDecoration(
                        hintText: "Route Name",
                        hintStyle: Theme.of(context).textTheme.bodyText1,
                        border: OutlineInputBorder()
                    ),
                    onSaved: (value) {
                      this.route.name = value;
                    },
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  this.route.location != null ?
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('${this.route.location.latitude ?? ' '}'),
                      Text('${this.route.location.longitude ?? ' '}'),
                      ElevatedButton(
                        onPressed: (){
                          _navigateToSelectLocation();},
                        style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),
                        child: Text('Change'),
                      )
                    ],
                  )
                  : ElevatedButton(
                    onPressed: (){
                      _navigateToSelectLocation();
                    },
                    style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Select Route Location"),
                      ],
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  this.route.location != null
                  ? this.isLoading
                      ? Center(child: SizedBox(height: 35, child: CircularProgressIndicator()))
                      : ElevatedButton(
                    onPressed: () {
                      _validateForm();
                    },
                    style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder()),

                    child: Center(
                      child: Text("Add Route",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 15
                        ),),
                    ),
                  )
                  : SizedBox(),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  _navigateToSelectLocation() async {
    Navigator.of(context)
        .push(MaterialPageRoute(
        builder: (context) => ChooseLocation()
    ))
        .then((value) {
      if(value != null) {
        if(value is LocationModel) {
          setState((){
            this.route.location = value;
          });
        }
      }
    });
  }
}

