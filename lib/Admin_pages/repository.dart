import 'package:http/http.dart' as http;
import 'dart:convert';


Future<void> sendNotification(String topicName, { String notificationBody = 'Test Body', String title = 'Test title'}) async {
  final FCM_SERVER_KEY = 'AAAAdp7Ph_Q:APA91bGS2-20eotvQ_y8jsOhZgUSbPYpdr3YHhJUkuInRct9QejWlBObLWb3EhMO3bOdtECuBPSb8OvikLyX9PrLyWHBP3eiXdFzPsbTPmiJryR5mNdLK21YAbp1e-JKT7lb5WeLk1gy';

  final url = 'https://fcm.googleapis.com/fcm/send';

  Map<String, String> headers = Map();
  headers['content-type'] = 'application/json';
  headers['Authorization'] = 'key=$FCM_SERVER_KEY';

  Map<String, dynamic> body = Map();
  body['notification'] = {
    'body' : notificationBody,
    'title': title,
  };
  body['priority'] = 'high';
  body['data'] = {
    'click_action': 'FLUTTER_NOTIFICATION_CLICK',
    'id': '1',
    'status': 'done',
    'screen': '$topicName',
    'sound': 'default'
  };
  // body['to'] = '/topic/$topicName';
  body['to'] = '/topics/$topicName';

  print(body);

  final result = await http.post(
      Uri.parse(url),
      headers: headers,
      body: json.encode(body),
      encoding: Encoding.getByName('utf-8')
  );
  print(result.body);
  print('Notification sent');

}
