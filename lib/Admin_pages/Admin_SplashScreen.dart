import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/Admin_pages/Admin_home.dart';
import 'package:zoe/Admin_pages/Admin_login.dart';
import 'package:zoe/auth/LoginIn.dart';
import 'package:zoe/model/user.dart';
class Admin_SplashScreen extends StatefulWidget {
  MyUser currentUser;
   Admin_SplashScreen({Key key, MyUser currentUser}) : super(key: key);

  @override
  _Admin_SplashScreenState createState() => _Admin_SplashScreenState();
}

class _Admin_SplashScreenState extends State<Admin_SplashScreen> {
  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() async {
    Future.delayed(Duration(milliseconds: 700), (){
      ///Get value and navigate
      bool isSignedIn = FirebaseAuth.instance.currentUser != null;
      if(isSignedIn) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => AdminHome()));
        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => WelcomePage()));
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Admin_login()));
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CupertinoIcons.settings, color: Colors.black,),

            SizedBox(height: 20),


            SizedBox(
              height: 35,
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );

  }
}
