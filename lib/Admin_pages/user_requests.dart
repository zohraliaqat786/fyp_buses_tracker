import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../model/user.dart';

class UserRequestsPage extends StatefulWidget {
  const UserRequestsPage({Key key}) : super(key: key);

  @override
  _UserRequestsPageState createState() => _UserRequestsPageState();
}

class _UserRequestsPageState extends State<UserRequestsPage> {
  StreamSubscription pendingSub;
  StreamSubscription activeSub;
  bool showPending = true;

  @override
  initState() {
    super.initState();
    getPendingUserRequests();
    getActiveUsers();
  }

  @override
  dispose() {
    pendingSub.cancel();
    activeSub.cancel();
    super.dispose();
  }

  List<MyUser> pending = [];
  List<MyUser> filteredPendingUsers = [];

  getPendingUserRequests() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routesRef = firestore.collection('users');
    pendingSub = routesRef
        .where('isActive', isEqualTo: false)
        .snapshots(includeMetadataChanges: false)
        .listen((snapshot) {
      if (snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _user = MyUser.fromJson(docSnap.data());
          if (!_user.isActive && _user.type != 'admin') {
            resetLists();
            pending.removeWhere((element) => element.id == _user.id);
            filteredPendingUsers
                .removeWhere((element) => element.id == _user.id);
            setState(() {
              pending.add(_user);
              filteredPendingUsers.add(_user);
            });
          }
        });
      }
    });
  }

  List<MyUser> active = [];
  List<MyUser> filteredActiveUsers = [];

  getActiveUsers() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routesRef = firestore.collection('users');
    activeSub = routesRef
        .where('isActive', isEqualTo: true)
        .snapshots(includeMetadataChanges: false)
        .listen((snapshot) {
      if (snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _user = MyUser.fromJson(docSnap.data());
          if (_user.isActive && _user.type != 'admin') {
            resetLists();
            active.removeWhere((element) => element.id == _user.id);
            filteredActiveUsers
                .removeWhere((element) => element.id == _user.id);
            setState(() {
              active.add(_user);
              filteredActiveUsers.add(_user);
            });
          }
        });
      }
    });
  }

  removeFromPending(MyUser _user) {
    pending.removeWhere((element) => element.id == _user.id);
    filteredPendingUsers.removeWhere((element) => element.id == _user.id);
    setState(() {});
  }

  removeFromActive(MyUser _user) {
    active.removeWhere((element) => element.id == _user.id);
    filteredActiveUsers.removeWhere((element) => element.id == _user.id);
    setState(() {});
  }

  toggleUserStatus(MyUser _user) {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference usersRef = firestore.collection('users');
    usersRef.doc(_user.id).update({'isActive': !_user.isActive});
  }

  filterUsers(String text) {
    filteredActiveUsers.clear();
    final _activeList = active.where((element) => element.name.toString().toLowerCase().contains(text.toLowerCase())).toList();
    filteredActiveUsers.addAll(_activeList);
    filteredPendingUsers.clear();
    final _pendingList = pending.where((element) => element.name.toString().toLowerCase().contains(text.toLowerCase())).toList();
    filteredPendingUsers.addAll(_pendingList);
    setState((){});

  }

  resetLists(){
    filteredActiveUsers.clear();
    filteredActiveUsers.addAll(active);
    filteredPendingUsers.clear();
    filteredPendingUsers.addAll(pending);
    setState((){});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Pending Users\' Requests"),
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: TextField(
                autofocus: false,
                autocorrect: false,
                keyboardType: TextInputType.text,
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                    hintText: "Search by name",
                    hintStyle: Theme.of(context).textTheme.bodyText1,
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      borderRadius: BorderRadius.circular(5)
                    )),
                onChanged: (value) {
                  if(value.isEmpty) {
                    resetLists();
                  } else {
                    filterUsers(value);
                  }
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (!this.showPending) {
                          setState(() {
                            this.showPending = true;
                          });
                          resetLists();
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                            border: this.showPending
                                ? Border(
                                    bottom: BorderSide(
                                        color: Colors.black, width: 1))
                                : null),
                        child: Center(
                          child: Text("Pending",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: this.showPending
                                      ? Colors.black
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (this.showPending) {
                          setState(() {
                            this.showPending = false;
                          });
                          resetLists();
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                            border: !this.showPending
                                ? Border(
                                    bottom: BorderSide(
                                        color: Colors.black, width: 1))
                                : null),
                        child: Center(
                          child: Text("Active",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: !this.showPending
                                      ? Colors.black
                                      : Colors.grey)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            this.showPending
                ? _buildPendingUsersList()
                : _buildActiveUsersList(),
          ],
        )));
  }

  Widget _buildPendingUsersList() {
    return this.filteredPendingUsers.isEmpty
        ? SizedBox()
        : ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: this.filteredPendingUsers.length,
            itemBuilder: (context, i) {
              MyUser _current = this.filteredPendingUsers[i];
              // return Text(i.toString());
              return buildUserItem(
                  _current.name,
                  _current.isDriver ? "Driver" : "Rider",
                  _current.isDriver
                      ? "CNIC: ${_current.CNIC}"
                      : "MIS ID: ${_current.MISid}",
                  _current.isActive, () {
                print("Mark as Active");
                removeFromPending(_current);
                toggleUserStatus(_current);
              });
            },
          );
  }

  Widget _buildActiveUsersList() {
    return this.filteredActiveUsers.isEmpty
        ? SizedBox()
        : ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: this.filteredActiveUsers.length,
            itemBuilder: (context, i) {
              MyUser _current = this.filteredActiveUsers[i];
              // return Text(i.toString());
              return buildUserItem(
                  _current.name,
                  _current.isDriver ? "Driver" : "Rider",
                  _current.isDriver
                      ? "CNIC: ${_current.CNIC}"
                      : "MIS ID: ${_current.MISid}",
                  _current.isActive, () {
                print("Mark as InActive");
                removeFromActive(_current);
                toggleUserStatus(_current);
              });
            },
          );
  }

  Widget buildUserItem(String name, String type, String subtitle, bool isActive,
      VoidCallback onTap) {
    return ListTile(
      leading: Icon(
        CupertinoIcons.person,
        color: Colors.black,
      ),
      trailing: TextButton(
        onPressed: () {
          onTap.call();
        },
        child: Text(
          isActive ? "Mark InActive" : "Mark Active",
          style: TextStyle(fontSize: 13, color: Colors.red),
        ),
      ),
      title: Text(name,
          style: TextStyle(
            color: Colors.black,
          )),
      subtitle: Text(
        '($type), $subtitle',
        style: TextStyle(color: Colors.black),
      ),
    );
  }
}
