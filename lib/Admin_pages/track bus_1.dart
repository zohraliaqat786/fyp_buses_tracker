import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/Rider_pages/Stop_Selection.dart';
import 'package:zoe/Rider_pages/bus_map.dart';
import 'package:zoe/auth/LoginIn.dart';
import 'package:zoe/model/BusRoute.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';
class busList_tracking extends StatefulWidget {
  MyUser currentUser;
     busList_tracking({Key key,this.currentUser}) : super(key: key);

  @override
  _busList_trackingState createState() => _busList_trackingState();
}

class _busList_trackingState extends State<busList_tracking> {
  MyUser currentUser;
  BusRoute _selectedRoute;
  List<Bus> buses = [];
  @override
  initState() {
    this.currentUser = widget.currentUser;
    super.initState();
    _getMyRouteBuses();
    //The getter 'stop' was called on null
    //Means we are calling .stop on a null variable
    //In our case, its currentUser. The value of CurrentUser is null here
    //subscribeNotificationsTopic();
  }
  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LogIn()), (route) => false);
  }
  _getMyRouteBuses() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference buses = firestore.collection('buses');
    //this.buses.clear();
    final ref = buses;
    ref.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _bus = Bus.fromJson(docSnap.data());
          setState(() {
            this.buses.add(_bus);
          });
        });
      }
    });
  }
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text('Bus Tracking'),
        //title: Text(' ${this.currentUser?.name ?? ''}'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Divider(thickness: 1,),

              Text('Select bus routes from list below', style: textTheme.bodyText1.apply(color: Color(0xff2b2a2a)),),
              SizedBox(height: 15,),
              this.buses.isEmpty
                  ? SizedBox()
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: this.buses.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('Bus No. (${this.buses[index].busNo})', style: textTheme.bodyText1,),
                    leading: Icon(CupertinoIcons.car_detailed, color: Colors.black,),
                    trailing: IconButton(
                      onPressed: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => BusMapPage(
                                  currentUser: this.currentUser,
                                  bus: this.buses[index],
                                )
                            )
                        );
                      },
                      icon: Icon(CupertinoIcons.location, color: Colors.black87,),
                    ),
                  );
                },
              )

            ],
          ),
        ),
      ),
    );
  }
}
