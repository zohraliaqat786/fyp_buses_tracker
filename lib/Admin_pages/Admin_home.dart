import 'package:back_pressed/back_pressed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/Admin_pages/AddBuss.dart';
import 'package:zoe/Admin_pages/Admin_login.dart';
import 'package:location/location.dart' as loc;
import 'package:zoe/Admin_pages/send%20notification.dart';
import 'package:zoe/Admin_pages/track%20bus_1.dart';
import 'package:zoe/Rider_pages/addmystop.dart';
import 'package:zoe/model/user.dart';
import '../changePassword.dart';
import 'user_requests.dart';
import 'AddRoute.dart';
import 'bus schedule.dart';

class AdminHome extends StatefulWidget {
  MyUser currentUser;

  AdminHome({Key key, MyUser currentUser}) : super(key: key);

  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  var _location = loc.Location();
  loc.PermissionStatus _status;
  bool isServiceEnabled = false;
  List<Marker> markers = [];
  GoogleMapController _mapController;
  String adresses = 'your adress here';
  MyUser currentUser;

  @override
  initState() {
    super.initState();
    _initialize();
  }

  _initialize() {
    Future.delayed(Duration(milliseconds: 1000), () {
      _getUserDetails();
      getCurrentLocation();
      _location.hasPermission().then(_updateStatus);
    });
  }

  _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    setState(() {
      this.currentUser = MyUser.fromJson(data);
    });
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Admin_login()),
        (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return OnBackPressed(
        perform: () {
          SystemNavigator.pop();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(adresses),
          ),
          body: Stack(
            children: [
              GoogleMap(
                initialCameraPosition:
                    CameraPosition(target: LatLng(40.7, -74), zoom: 13),
                onTap: (position) {
                  print('tapped on $position');
                  _getAddress(position);
                },
                markers: Set.from(this.markers),
                onMapCreated: (GoogleMapController controller) {
                  _mapController = controller;
                },
              ),
              //Align(alignment: Alignment.topCenter,
              //child: Container(
              //height: 40,
              //width: MediaQuery.of(context).size.width,
              //color: Colors.white,
              //padding: EdgeInsets.all(10),
              //margin: EdgeInsets.only(top: 0, left: 0, right: 0),
              //  child: Text(adresses),
              //  ),
              //)
            ],
          ),
          drawer: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: Color(
                  0xff2b2a2a), //This will change the drawer background to blue.
            ),
            child: Drawer(
              child: ListView(
                children: [
                  Container(
                    height: 125,
                    child: DrawerHeader(
                      decoration: BoxDecoration(color: Color(0xff2b2a2a)),
                      child: Row(
                        children: [
                          //SizedBox(height: 20),
                          Image.asset("assets/imge/kindpng.png",
                              height: 65, width: 64),
                          SizedBox(width: 16),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 20),
                              Text(
                                'Welcome ',
                                style: TextStyle(
                                    fontSize: 22.0,
                                    color: Colors.white,
                                    fontFamily: "Brand-Bold"),
                              ),
                              SizedBox(height: 6),
                              Text(
                                "Admin",
                                style: TextStyle(
                                    fontSize: 19.0, color: Colors.white),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    thickness: 1,
                  ),
                  //DividerWidget(),
                  SizedBox(height: .0),
                  ListTile(
                    leading:
                        Icon(Icons.location_on_rounded, color: Colors.white),
                    onTap: () {
                      //Have to pass currentUser's value here
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => busList_tracking(
                                currentUser: this.currentUser,
                              )));
                    },
                    title: Text(
                      "track bus",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.schedule, color: Colors.white),
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => AddRoute()));
                    },
                    title: Text(
                      "Add Route",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.directions_bus, color: Colors.white),
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => AddBuss()));
                    },
                    title: Text(
                      "Add Bus",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.request_page, color: Colors.white),
                    onTap: () {
                      print("Navigating to Req page");
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => UserRequestsPage()));
                    },
                    title: Text(
                      "Requests",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.schedule_outlined, color: Colors.white),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => BusScheduleWidget()));
                    },
                    title: Text(
                      "Add bus Schedule",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.notifications, color: Colors.white),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SendNotificationWidget()));
                    },
                    title: Text(
                      "Notification",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.security, color: Colors.white),
                    onTap: () {
                      //Have to pass currentUser's value here
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ChangePassword()));
                    },
                    title: Text(
                      "Change Password",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.logout, color: Colors.white),
                    onTap: () {
                      _logout();
                    },
                    title: Text(
                      "Logout",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.close, color: Colors.white),
                    onTap: () {
                      //Have to pass currentUser's value here
                      Navigator.pop(context, true);
                    },
                    title: Text(
                      "close",
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.my_location_outlined),
            onPressed: () {
              getCurrentLocation(askPermission: true);
            },
          ),
        ));
  }

  void _updateStatus(loc.PermissionStatus _status) {
    if (_status != loc.PermissionStatus.granted) {
      isServiceEnabled = false;
      //TODO Ask for permision
      _askPermission();
    } else {
      setState(() {
        this._status = _status;
      });
      _requestLocationServices();
    }
  }

  void _askPermission() {
    if (_status == null) {
      _location.requestPermission().then((status) {
        if (status == loc.PermissionStatus.granted) {
          setState(() {
            this._status = status;
            //other work here
          });
        }
        //check for location Services Status
        _requestLocationServices();
      });
    }
  }

  void _requestLocationServices() async {
    if (await _location.serviceEnabled()) {
      isServiceEnabled = true;
      //todo:get users current location
      getCurrentLocation();
    } else {
      isServiceEnabled = true;
      var enabled = await _location.requestService();
      if (enabled) {
        //todo: get user current  location
        getCurrentLocation();
      }
    }
  }

  void getCurrentLocation({bool askPermission = false}) async {
    if (isServiceEnabled && this._status == loc.PermissionStatus.granted) {
      //get current location
      loc.LocationData locationData = await _location.getLocation();
      LatLng position =
          LatLng(locationData.latitude ?? 40.7, locationData.longitude ?? -74);
      //todo: get user Address here
      _getAddress(position);
    } else if (askPermission) {
      _askPermission();
    }
  }

  void _getAddress(LatLng position) async {
    final coordinates = Coordinates(position.latitude, position.longitude);
    //TODO get location address
    final addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      this.adresses = addresses.first.featureName +
          " " +
          addresses.first.subAdminArea +
          " " +
          addresses.first.countryName;
    });

    //TODO add marker
    _addNewMarker(position, adresses);
  }

  void _addNewMarker(LatLng position, String adresses) {
    Marker _newMarker = Marker(
        markerId: MarkerId('currentUser'),
        position: position,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
        infoWindow: InfoWindow(
            title: 'you are here', snippet: adresses ?? 'your location'));
    setState(() {
      this.markers.clear();
      this.markers.add(_newMarker);
    });
    if (_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newLatLng(position));
    }
  }
}
