import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/auth/LoginIn.dart';
import 'package:zoe/model/BusRoute.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';

import 'Stop_Selection.dart';
import 'bus_map.dart';
class Addmystop extends StatefulWidget {
  MyUser currentUser;
   Addmystop({Key key,this.currentUser}) : super(key: key);

  @override
  _AddmystopState createState() => _AddmystopState();
}

class _AddmystopState extends State<Addmystop> {
  MyUser currentUser;
  BusRoute _selectedRoute;
  List<Bus> buses = [];

  @override
  initState() {
    this.currentUser = widget.currentUser;
    super.initState();
    //The getter 'stop' was called on null
    //Means we are calling .stop on a null variable
    //In our case, its currentUser. The value of CurrentUser is null here
    if(this.currentUser.stop != null) {
      //Get Stop then
      _getStopDetails(this.currentUser.stop);
    }
    //subscribeNotificationsTopic();
  }
  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LogIn()), (route) => false);
  }
  Future<MyUser> _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    setState((){
      this.currentUser = MyUser.fromJson(data);
    });
    if(this.currentUser.stop != null) {
      //Get Stop then
      _getStopDetails(this.currentUser.stop);
    }
  }

  _getStopDetails(var stopId) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routes = firestore.collection('routes');
    final _routeDetails = await routes.doc(stopId).get();
    Map data = _routeDetails.data();
    setState((){
      this._selectedRoute = BusRoute.fromJson(data);
    });

    //Subscribe to FCM Topic
    //FirebaseMessaging messaging = FirebaseMessaging.instance;
    //messaging.subscribeToTopic(this._selectedRoute.id);

    _getMyRouteBuses();
  }

  _getMyRouteBuses() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference buses = firestore.collection('buses');
    this.buses.clear();
    final ref = buses.where('stops', arrayContains: this._selectedRoute.id);
    ref.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _bus = Bus.fromJson(docSnap.data());
          setState(() {
            this.buses.add(_bus);
          });
        });
      }
    });
  }





  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        //title: Text(' ${this.currentUser?.name ?? ''}'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              _selectedRoute == null
                  ? _selectStopButton()
                  : _stopIndicator(),

              Divider(thickness: 1,),

              this.buses.isEmpty
                  ? SizedBox()
                  : Text(
                'Buses visiting your Stop',
                style: textTheme.subtitle2.apply(color: Color(0xff2b2a2a)),
              ),
              SizedBox(height: 15,),
              this.buses.isEmpty
                  ? SizedBox()
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: this.buses.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('Bus No. (${this.buses[index].busNo})', style: textTheme.bodyText1,),
                    leading: Icon(CupertinoIcons.car_detailed, color: Colors.black,),
                    trailing: IconButton(
                      onPressed: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => BusMapPage(
                                  currentUser: this.currentUser,
                                  bus: this.buses[index],
                                )
                            )
                        );
                      },
                      icon: Icon(CupertinoIcons.location, color: Colors.black87,),
                    ),
                  );
                },
              )

            ],
          ),
        ),
      ),
    );

  }

  Widget _selectStopButton() {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return InkWell(
      onTap: (){
        // StopSelection
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StopSelection())).then((value) {
          if(value != null) {
            if(value) {
              _getUserDetails();
            }
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 0.5, color: Colors.white),
            borderRadius: BorderRadius.circular(3)
        ),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [

            Text('Select your Stop', style: textTheme.subtitle1.apply(color: Color(0xff2b2a2a))),

            Spacer(),

            IconButton(icon: Icon(CupertinoIcons.forward, color: Colors.black,),
                onPressed: (){})

          ],
        ),
      ),
    );
  }

  Widget _stopIndicator() {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Your selected Stop', style: textTheme.subtitle1.apply(color: Color(0xff2b2a2a)),),
        SizedBox(),
        Row(
          children: [
            Text('${this._selectedRoute.name ?? ''}', style: textTheme.bodyText1.apply(color: Color(0xff2b2a2a)),),
            Spacer(),
            IconButton(icon: Icon(CupertinoIcons.pen, color: Colors.black,), onPressed: (){
              // StopSelection
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => StopSelection(selectedRoute: this._selectedRoute,))).then((value) {
                if(value != null) {
                  if(value) {
                    _getUserDetails();
                  }
                }
              });
            })
          ],
        ),
      ],
    );
  }

}
