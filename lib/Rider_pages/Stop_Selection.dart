import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zoe/model/BusRoute.dart';
class StopSelection extends StatefulWidget {
  BusRoute selectedRoute;
  StopSelection({Key key, BusRoute selectedRoute}) : super(key: key);

  @override
  _StopSelectionState createState() => _StopSelectionState();
}

class _StopSelectionState extends State<StopSelection> {
  List<BusRoute> _routes = [];
  BusRoute _previousRoute;
  BusRoute _selectedRoute;
  bool isLoading = false;

  @override
  initState() {
    this._selectedRoute = widget.selectedRoute;
    this._previousRoute = widget.selectedRoute;
    super.initState();
    _getRoutes();
  }

  _getRoutes() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routesRef = firestore.collection('routes');
    routesRef.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _route = BusRoute.fromJson(docSnap.data());
          setState(() {
            _routes.add(_route);
          });
        });
        //getUserLocation();
      }
    });
  }
  _updateRoute() async {
    if(this._selectedRoute != null) {
      setState(() {
        this.isLoading = true;
      });
      try {
        final _authUser = FirebaseAuth.instance.currentUser;
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        CollectionReference users = firestore.collection('users');
        users.doc(_authUser.uid).update({'stop': _selectedRoute.id});

        //Subscribe to FCM Topic
        //FirebaseMessaging messaging = FirebaseMessaging.instance;
        //try {
          //messaging.unsubscribeFromTopic(this._previousRoute.id);
        //} catch (e) {

        //}
        //messaging.subscribeToTopic(this._selectedRoute.id);

        setState(() {
          this.isLoading = false;
        });
        Navigator.of(context).pop(true);
      } catch (e) {
        print(e);
        setState(() {
          this.isLoading = false;
        });
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Please select a route")));
    }
  }


  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text('Select your route'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(height: 15,),
                //Column(
                 // children: [

                   // Text('Your nearest Route/Stop', style: textTheme.bodyText1,),

                    //SizedBox(height: 15,),
                  //],
                //),
                   // : SizedBox(),

                Text('Select bus routes from list below', style: textTheme.bodyText1.apply(color: Color(0xff2b2a2a)),),
                SizedBox(height: 5),
                _routes.isEmpty
                    ? SizedBox()
                    : ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: _routes.length,
                  itemBuilder: (context, index) {

                    final route = _routes[index];

                    final _isInSelected = _selectedRoute != null && _selectedRoute.id == route.id;
                    var _respectiveDistance = 0.0;


                    return ListTile(
                      title: Text('${route.name}', style: textTheme.bodyText2,),
                      subtitle: Text('${_respectiveDistance.toStringAsFixed(2)} KMs', style: textTheme.caption,),
                      trailing: myCheckbox(_isInSelected),
                      onTap: (){
                        if(_isInSelected) {
                          setState(() {
                            this._selectedRoute = null;
                          });
                        } else {
                          setState(() {
                            this._selectedRoute = route;
                          });
                        }
                      },
                    );
                  },
                ),

                SizedBox(
                  height: 15,
                ),

                this.isLoading
                    ? Center(child: SizedBox(height: 35, child: CircularProgressIndicator()))
                    : ElevatedButton(
                  onPressed: () {
                    _updateRoute();
                  },
                  style: ElevatedButton.styleFrom(primary:Color(0xff2b2a2a),shape: StadiumBorder(),
                  ),
                  child: Center(
                    child: Text("Update",),
                  ),
                ),



              ],
            ),
          ),
        ),
      ),

    );


}

  Widget myCheckbox(bool isChecked) {
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          border: Border.all(color: Colors.green, width: 0.8),
          color: isChecked ? Colors.green : Colors.transparent
      ),
      child: Center(
        child: isChecked ? Icon(
          CupertinoIcons.checkmark_alt,
          color: Colors.black,
          size: 14,
        ) : SizedBox(),
      ),
    );
  }

}
