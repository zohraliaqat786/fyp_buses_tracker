import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/model/BusRoute.dart';
import 'package:zoe/model/user.dart';
import 'package:location/location.dart' as loc;

class SearchNearestStop extends StatefulWidget {
  MyUser currentUser;

  SearchNearestStop({Key key, this.currentUser}) : super(key: key);

  @override
  _SearchNearestStopState createState() => _SearchNearestStopState();
}

class _SearchNearestStopState extends State<SearchNearestStop> {
  loc.PermissionStatus _status;
  bool isServiceEnabled = false;
  var _location = loc.Location();
  List<Marker>markers = [];
  GoogleMapController _mapController;
  String adresses = 'your adress here';
  LatLng currentPosition;

  List<BusRoute> stops = [];
  List<BusRoute> stopsNearby = [];
  
  @override
  initState(){
    super.initState();
    _location.hasPermission().then(_updateStatus);
  }

  _getNearestStops() async {
    stopsNearby.clear();
    stops.clear();
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    final _snaps = await _firestore.collection('routes').get();
    if(_snaps.size > 0) {
      final _docs = _snaps.docs;
      if(_docs.isNotEmpty) {
        for(int i = 0; i<_docs.length; i++) {
          final _doc = _docs[i];
          final _data = _doc.data();
          BusRoute _route = BusRoute.fromJson(_data);
          _route.distance = calculateDistance(this.currentPosition.latitude,
              this.currentPosition.longitude,
            _route.location.latitude,
            _route.location.longitude
          );
          this.stops.add(_route);
        }
        //Sort Routes & get only first 3 (if stops > 3)
        this.stops.sort((a, b) => a.distance > b.distance ? 1 : 0);
        if(this.stops.length <= 3) {
          setState(() {
            this.stopsNearby.addAll(this.stops);
          });
        } else {
          for(int i = 0; i<3; i++) {
            this.stopsNearby.add(this.stops[i]);
          }
        }
        //Add Route Markers with distance
        for(int i = 0; i<this.stopsNearby.length; i++) {
          final latlong = LatLng(this.stopsNearby[i].location.latitude,
              this.stopsNearby[i].location.longitude
          );
          final _title = '${this.stopsNearby[i].name} (${this.stopsNearby[i].distance.toStringAsFixed(1)} KMs)';
          await _addNewStopMarker(
              latlong,
              _title
          );
        }
      }
    }
  }

  double calculateDistance(double lat1, double lon1, double lat2, double lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text( ""),
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(initialCameraPosition: CameraPosition(
                  target: LatLng(40.7, -74), zoom: 13),
                onTap: (position) {
                  print('tapped on $position');
                  //_getAddress(position);
                },
                markers: Set.from(this.markers),
                onMapCreated: (GoogleMapController controller) {
                  _mapController = controller;
                },),
            ),
          ],
        ),
      ),
    );
  }

  void _updateStatus(loc.PermissionStatus _status) {
    if (_status != loc.PermissionStatus.granted) {
      isServiceEnabled = false;
      //TODO Ask for permision
      _askPermission();
    } else {
      setState(() {
        this._status = _status;
      });
      _requestLocationServices();
    }
  }

  void _askPermission() {
    if (_status == null) {
      _location.requestPermission().then((status) {
        if (status == loc.PermissionStatus.granted) {
          setState(() {
            this._status = _status;
            //other work here
          });
        }
        //check for location Services Status
        _requestLocationServices();
      });
    }
  }

  void _requestLocationServices() async {
    if (await _location.serviceEnabled()) {
      isServiceEnabled = true;
      //todo:get users current location
      getCurrentLocation();
    } else {
      isServiceEnabled = true;
      var enabled = await _location.requestService();
      if (enabled) {
        //todo: get user current  location
        getCurrentLocation();
      }
    }
  }

  void getCurrentLocation() async {
    if (isServiceEnabled && this._status == loc.PermissionStatus.granted) {
      //get current location
      loc.LocationData locationData = await _location.getLocation();
      currentPosition =
          LatLng(locationData.latitude ?? 40.7, locationData.longitude ?? -74);
      //todo: get user Address here
      _getAddress(currentPosition);
    }
  }

  void _getAddress(LatLng position) async {
    //TODO get location address
    final addresses = await getAddresses(position.latitude, position.longitude);
    setState(() {
      this.adresses = addresses.first.featureName +
          " " +
          addresses.first.subAdminArea +
          " " +
          addresses.first.countryName;
    });

    //TODO add marker
    _addNewMarker(position, adresses);
  }

  void _addNewMarker(LatLng position, String adresses) {
    Marker _newMarker = Marker(
        markerId: MarkerId('currentUser'),
        position: position,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        infoWindow: InfoWindow(
            title: 'you are here', snippet: adresses ?? 'your location'));
    setState(() {
      this.markers.clear();
      this.markers.add(_newMarker);
    });
    if (_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newLatLng(position));
    }
    _getNearestStops();
  }

  Future<List<Address>> getAddresses (double lat, double long) async {
    final coordinates = Coordinates(lat, long);
    //TODO get location address
    final _addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    return _addresses;
  }

  void _addNewStopMarker(LatLng position, String stopName) async {
    String address = '';
    //TODO: Get Stop address here
    final _addresses = await getAddresses(position.latitude, position.longitude);
    address = _addresses.first.featureName +
    " " +
        _addresses.first.subAdminArea +
    " " +
        _addresses.first.countryName;
    Marker _newMarker = Marker(
        markerId: MarkerId('$stopName'),
        position: position,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
        infoWindow: InfoWindow(
            title: stopName, snippet: address ?? 'Stop location'));
    setState(() {
      this.markers.add(_newMarker);
    });
    if (_mapController != null) {
      //_mapController.animateCamera(CameraUpdate.newLatLng(position));
    }
  }
}
