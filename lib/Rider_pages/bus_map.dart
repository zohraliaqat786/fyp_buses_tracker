import 'dart:typed_data';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zoe/model/buss.dart';
import 'package:zoe/model/user.dart';
import 'package:location/location.dart' as l;
class BusMapPage extends StatefulWidget {
  MyUser currentUser;
  Bus bus;
  BusMapPage({Key key, this.currentUser, this.bus}) : super(key: key);

  @override
  _BusMapPageState createState() => _BusMapPageState();
}

class _BusMapPageState extends State<BusMapPage> {
  MyUser currentUser;
  Bus bus;
  BitmapDescriptor customicon;

  List<Marker> allMarkers =  []; //0 index => User's Marker || 1 index => Bus Marker
  GoogleMapController _mapController;



  @override
  void initState() {
    initStyle();
    this.currentUser = widget.currentUser;
    this.bus = widget.bus;

    super.initState();
    checkPermissions();
    listenBusLocationChanges();
  }

  checkPermissions() async {
    if(await getLocationPermission()) {
      setLocationListener();
    }
  }

  listenBusLocationChanges() async {
    FirebaseFirestore.instance
        .collection("buses")
        .doc(this.bus.id)
        .snapshots()
        .listen((snap) {
      if(mounted) {

        if(snap.exists) {

          final data = snap.data();

          if(data['currentLocation'] != null) {
            //Get GeoPoint and convert to LatLng
            //Add Marker
            final _busLocation = data['currentLocation'] as GeoPoint;

            l.LocationData _loc = l.LocationData.fromMap({
              'latitude': _busLocation.latitude,
              'longitude': _busLocation.longitude
            });

            addMarker(_loc, 1, title: 'Bus No. ${this.bus.busNo}');

          }

        }

      }
    });
  }


  @override
  Widget build(BuildContext context) {
    //createMarker(context);
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: [

          Container(
            width: size.width,
            height: size.height,
            // color: Colors.indigo,
            child: GoogleMap(
              onMapCreated: (GoogleMapController _controller) {
                this._mapController = _controller;
                setMapStyle(mapStyle);
              },
              initialCameraPosition: CameraPosition(
                  target: LatLng(33.6780513, 73.1843443), zoom: 13
              ),
              onTap: (post){},
              //mapType: MapType.hybrid,
              markers: Set.from(this.allMarkers),
            ),
          ),

          Positioned(
            top: 50,
            right: 10,
            left: 10,
            child: Text(
                'Tracking location of Bus No. ${this.bus.busNo ?? ''}'
            ),
          )



        ],
      ),
    );

  }

  Future<bool> getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
      ///Granted
      ///We can Update location
      return true;
    } else {
      final _req = await l.Location().requestPermission();
      if(_req == l.PermissionStatus.granted) {
        ///Granted
        ///We can Update location
        return true;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: Text("You must allow permission to update location")
            )
        );
        return false;
      }
    }
  }

  setLocationListener() async {
    final listener = l.Location.instance.onLocationChanged;
    listener.listen((l.LocationData currentLocation) {
      if(mounted) {

        //Add Marker on Map
        addMarker(currentLocation, 0);

      }
    });
  }

  Marker lastBusMarker;
  addMarker(l.LocationData loc, int index, { String title}) async {
    ImageConfiguration configuration =createLocalImageConfiguration(context);
    final _id = UniqueKey();
    if(index == 1) {
      final _customIcon = await getBytesFromAsset('assets/imge/bus.png', 60);
      lastBusMarker = Marker(
          markerId: MarkerId('Driver location'),
          draggable: false,
          rotation: loc.heading,
          infoWindow: InfoWindow(title: title ?? 'You are here', snippet: ''),
          position: LatLng(loc.latitude, loc.longitude),
          icon: BitmapDescriptor.fromBytes(_customIcon)
      );

      setState(() {
        // this.allMarkers.add(_marker);
        // this.allMarkers[index] = _marker;
        if(allMarkers.length != 0) {
          this.allMarkers.insert(index, lastBusMarker);
          lastBusMarker = null;
        }
      });
    } else {
      final _marker = Marker(
          markerId: MarkerId('StudentLocation'),
          draggable: false,
          infoWindow: InfoWindow(title: title ?? 'You are here', snippet: ''),
          position: LatLng(loc.latitude, loc.longitude),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue)
      );
      setState(() {
        // this.allMarkers.add(_marker);
        // this.allMarkers[index] = _marker;
        this.allMarkers.insert(index, _marker);
        if(lastBusMarker != null) {
          this.allMarkers.insert(1, lastBusMarker);
          lastBusMarker = null;
        }
      });
    }
    
    if(index == 1) {
      moveCamera(loc);
    }
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }



  moveCamera(l.LocationData _loc) {
    // LatLng latLng_1 = LatLng(40.416775, -3.70379);
    // LatLng latLng_2 = LatLng(41.385064, 2.173403);
    // LatLngBounds bound = LatLngBounds(southwest: latLng_1, northeast: latLng_2);
    // CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);
    // this._mapController.animateCamera(u2).then((void v){
    //   check(u2,this.mapController);
    // });

    if(_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
              _loc.latitude,
              _loc.longitude
          ),
          zoom: 13,
        ),
      ),);
    }
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    _mapController.animateCamera(u);
    LatLngBounds l1=await c.getVisibleRegion();
    LatLngBounds l2=await c.getVisibleRegion();
    print(l1.toString());
    print(l2.toString());
    if(l1.southwest.latitude==-90 ||l2.southwest.latitude==-90)
      check(u, c);
  }



  ///MapStyling
  String mapStyle = "";
  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  initStyle() async {
    rootBundle.loadString('assets/map_style.json').then((value) {
      mapStyle = value;
    });
  }
  void setMapStyle(String mapStyle) {
    _mapController.setMapStyle(mapStyle);
  }

}
