class MyUser {

  var id;
  var name;
  var email;
  var MISid;
  var password;
  var type;
  var stop;
  bool isDriver = false;
  bool isActive = false;
  var CNIC;
  //var universityName;

  MyUser({
    this.id, this.password, this.email, this.name,this.MISid,this.type, this.isDriver = false,
    this.isActive = false,
  });
  MyUser.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.name = data['name'];
    this.email = data['email'];
    this.MISid = data['MISid'];
    this.CNIC = data['CNIC'];
    //this.universityName=data['universityName'];
    this.isDriver = data['isDriver'];
    this.isActive = data['isActive'];
    this.type = data['type'] != null ? data['type'] : null;
    this.stop = data['stop'] != null ? data['stop'] : null;
  }
  Map<String, dynamic> toMap(){
    Map<String, dynamic> data = Map();
    data['name'] = this.name;
    data['email'] = this.email;
    data['id'] = this.id;
    data['MISid'] = this.MISid;
    data['CNIC'] = this.CNIC;
    data['type']=this.type;
    data['isDriver'] = this.isDriver;
    data['isActive'] = this.isActive;
    return data;
  }
}