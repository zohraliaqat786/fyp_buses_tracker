class MyNotification {
  String id;
  String title;
  String body;
  bool forDriver;
  bool forRider;
  int time;

  MyNotification({
    this.id,
    this.title, this.body, this.forDriver = false,
    this.forRider = false,
    this.time,
});

  MyNotification.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.title = data['title'];
    this.body = data['body'];
    this.forDriver = data['forDriver'];
    this.forRider = data['forRider'];
    this.time = data['time'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map();
    data['id'] = id;
    data['title'] = title;
    data['body'] = body;
    data['forDriver'] = forDriver;
    data['forRider'] = forRider;
    data['time'] = this.time ?? DateTime.now().millisecondsSinceEpoch;
    return data;
  }

}