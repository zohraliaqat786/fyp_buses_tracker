import 'package:cloud_firestore/cloud_firestore.dart';

class BusRoute {

  var id;
  var name;

  LocationModel location;

  double distance = 0;

  BusRoute();

  BusRoute.fromJson(Map<String, dynamic> data){
    this.id = data['id'];
    this.name = data['name'];
    if(data['location'] != null) {
      GeoPoint geoPoint = data['location'] as GeoPoint;
      this.location = LocationModel();
      this.location.longitude = geoPoint.longitude;
      this.location.latitude = geoPoint.latitude;
    }
  }

  Map<String, dynamic> toMap(){
    Map<String, dynamic> data = Map();
    data['id'] = this.id;
    data['name'] = this.name;
    if(this.location != null) {
      GeoPoint geoPoint = GeoPoint(
          this.location.latitude, this.location.longitude);
      data['location'] = geoPoint;
    }
    return data;
  }

}

class LocationModel {
  var latitude;
  var longitude;

  LocationModel({this.latitude, this.longitude});

}
